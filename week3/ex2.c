#include <stdio.h> 
#define ALEN(x)  (sizeof(x) / sizeof((x)[0]))

void swap(int *a, int *b)
{
    int t = *a;
    *a = *b;
    *b = t;
}


void bubble_sort(size_t size, int arr[]) 
{
    for (int i = 0; i < size; ++i)
    {
        for (int j = 0; j < size - i - 1; ++j)
        {
            if (arr[j] > arr[j+1])
            {
                swap(&arr[j], &arr[j+1]);
            }
        }
    }
}

int main() 
{
    int arr[] = {9, 8, 7, 6, 5, 4, 3, 2, 1};

    bubble_sort(ALEN(arr), arr);

    for (int i = 0; i < ALEN(arr); ++i)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}