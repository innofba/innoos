#include <stdio.h> 
#include <stdlib.h>

typedef struct Node {
    int value;
    struct Node *next;
} Node;

void insert_node(Node **head, int val) {
    Node *new = (Node*) malloc(sizeof(Node));
    new->value = val;
    new->next = *head;
    *head = new;
}


void print_list(Node *head) {
    while(head != NULL) {
        printf("%d\n", head->value);
        head = head->next;
    }
}

void delete_node(Node **head, int val) {
    if (*head == NULL)
    {
        return;
    }
    Node *t = *head;
    if (t->value == val)
    {
        *head = t->next;
        return;
    }
    Node *t1 = t->next;
    while(t1 != NULL) {
        if (t1->value == val)
        {
            t->next = t1->next;
            return;
        }
        t = t1;
          t1 = t1->next;
    }
}

int main() {
    Node *head = NULL;
    insert_node(&head, 0);
    insert_node(&head, 1);
    insert_node(&head, 2);
    insert_node(&head, 3);
    insert_node(&head, 4);
    print_list(head);
    delete_node(&head, 1);
    print_list(head);
    return 0;
}

