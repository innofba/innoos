#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

int main(int argc, char *argv[]) {
    int fd, flags, opt;
    ssize_t nread, nwrite, nwrite2;
    char buf[1025];
    int has_file = 0;
    if (argc > 1)
        has_file = 1;

    if (has_file) {
        flags = O_WRONLY | O_CREAT | O_TRUNC;
        while ((opt = getopt(argc, argv, "a")) != -1) {
            switch (opt) {
                case 'a':
                    flags = O_WRONLY | O_APPEND | O_CREAT;
                    break;
                default:
                    fprintf(stderr, "Usage: %s filename [-a]\n", argv[0]);
                    exit(EXIT_FAILURE);
            }
        }

        fd = open(argv[argc-1], flags, 0644);

        if (fd == -1) {
            perror("open");
            return EXIT_FAILURE;
        }
    }

    while ((nread = read (STDIN_FILENO, buf, 1024)) != 0) {
        if (nread == -1) {
            if (errno == EINTR)
                continue;
            perror ("read");
            break;
        }
        nwrite = write(STDOUT_FILENO, buf, nread);
        if (nwrite == -1) {
            perror("write");
            exit(EXIT_FAILURE);
        }
        if (has_file) {
            nwrite2 = write(fd, buf, nread);
            if (nwrite2 == -1) {
                perror("write");
                exit(EXIT_FAILURE);
            }
        }
    }

    return 0;
}