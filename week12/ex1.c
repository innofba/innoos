#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main() {
    int rnd = open("/dev/random", O_RDONLY);
    FILE *f = fopen("ex1.txt", "w");

    char rndData[20];
    ssize_t result = read(rnd, rndData, sizeof rndData);

    for (int i = 0; i < 20; ++i)
    {
        rndData[i] = 33 + (rndData[i] % 93);
    }

    fprintf(f, rndData);

    return 0;
}