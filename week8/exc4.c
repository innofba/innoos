#include <stdio.h>
#include <stdlib.h>
#include <sys/resource.h>
#include <string.h>

int main() {
    struct rusage m_usage;
    int i;
    for (i = 0; i < 10; i++) {
        void *p = malloc(10 * 1024 * 1024);
        memset(p, 0, 10 * 1024 * 1024);
        getrusage(RUSAGE_SELF, &m_usage);
        printf("%ld\n", m_usage.ru_maxrss);
        sleep(1);
    }
    return 0;
}
