- physical memory address represent the real address of RAM memory, while in virtual memory 
all memory access should go through page table
- virtual memory stored on hard drive and used when RAM is full
- physical memory is RAM
- physical memory is faster
- virtual memory is a mapping to the actual physical address
- virtual memory gives the programmer an illusion of having 'infinite' memory
- virtual memory isolates processes with its own virtual address space
