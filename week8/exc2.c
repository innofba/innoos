#include <stdio.h>
#include <stdlib.h>
#include <sys/resource.h>
#include <string.h>

int main() {
    int i;
    for (i = 0; i < 10; i++) {
        void *p = malloc(10 * 1024 * 1024);
        memset(p, 0, 10 * 1024 * 1024);
        sleep(1);
        free(p);
    }
    return 0;
}
