#include<stdio.h>
#include<signal.h>
#include<unistd.h>

void onsigint(int signal) {
    if (signal == SIGINT)
        printf("It's SIGINT\n");
    else if (signal == SIGUSR1)
    	printf("It's SIGUSR1\n");
    else if (signal == SIGSTOP)
    	printf("It's SIGSTOP\n");
    else
        printf("It's %d\n", signal);
}

int main() {
    signal(SIGINT, onsigint);
    signal(SIGSTOP, onsigint);
    signal(SIGUSR1, onsigint);
    while (1) {
        sleep(1);
    }
    return 0;
}