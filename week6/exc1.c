#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <memory.h>
#include <stdlib.h>

int main() {
    char *str = "Original string";
    char copy_str[1024];

    int fd[2];

    pipe(fd);

    write(fd[1], str, strlen(str)+1);
    read(fd[0], copy_str, 1024);
    printf("%s\n", copy_str);

    return 0;
}