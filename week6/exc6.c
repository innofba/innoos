#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <memory.h>
#include <stdlib.h>
#include <signal.h>

int main() {
    pid_t child1_pid, child2_pid;

    int fd[2];
    pipe(fd);

    child1_pid = fork();
    if (child1_pid == 0) {
        read(fd[0], &child2_pid, sizeof(child2_pid));
        sleep(3);
        kill(child2_pid, SIGSTOP);
        sleep(3);
        kill(child2_pid, SIGCONT);
        sleep(3);
        kill(child2_pid, SIGTERM);
        return 0;
    }

    child2_pid = fork();
    if (child2_pid == 0) {
        while(1) {
            sleep(1);
        }
        return 0;
    }

    write(fd[1], &child2_pid, sizeof(child2_pid));

    for (int i = 0; i < 3; ++i) {
        int status;
        waitpid(child2_pid, &status, WCONTINUED | WUNTRACED);
        if (WIFCONTINUED(status))
            printf("Process continued\n");
        if (WIFSTOPPED(status))
            printf("Process stopped\n");
        if (WIFSIGNALED(status))
            printf("Process terminated\n");
    }
    return 0;
}