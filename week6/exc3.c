#include<stdio.h>
#include<signal.h>
#include<unistd.h>

void onsigint(int signal) {
    if (signal == SIGINT)
        printf("It's SIGINT\n");
    else
        printf("It's %d\n", signal);
}

int main() {
    signal(SIGINT, onsigint);
    while (1) {
        sleep(1);
    }
    return 0;
}