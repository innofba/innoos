#include <stdio.h> 
#include <sys/types.h> 
#include <unistd.h> 


int main() 
{
	int n = 42;
	pid_t pid = fork();

	switch(pid) {
		case -1:
			// Error while fork
			printf("ERROR\n");
			break;
		case 0:
			// child process pid is always 0
			printf("Hello from child [%d - %d]\n", pid, n);
			break;
		default:
			// parent process
			printf("Hello from parent [%d - %d]\n", pid, n);
	}
    return 0; 
}