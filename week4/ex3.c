#include <stdio.h> 
#include <sys/types.h> 
#include <unistd.h> 
#include <string.h>
#include <stdlib.h>

int main() 
{
	while (1) {
		printf("> ");

		char *cmd;
		scanf("%s", cmd);

		if (strcmp(cmd, "exit") == 0)
			break;

		system(cmd);
	}
	return 0;
}