#include <stdio.h> 
#include <sys/types.h> 
#include <unistd.h> 

// program will produce 2^3 processes, 'cause child 
// process will also do fork which will form binary tree of processes

int main() 
{
	for (int i = 0; i < 3; ++i)
	{
		fork();
	}
	sleep(5);
	return 0;
}

// pstree:
//
// += 13194 fba ./ex2
// |-+- 13195 fba ./ex2
// | |-+- 13198 fba ./ex2
// | | \--- 13201 fba ./ex2
// | \--- 13199 fba ./ex2
// |-+- 13196 fba ./ex2
// | \--- 13200 fba ./ex2
// \--- 13197 fba ./ex2