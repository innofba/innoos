#include <stdio.h> 
#include <sys/types.h> 
#include <unistd.h> 
#include <string.h>
#include <stdlib.h>

int main() 
{
	printf("> ");
	while (1) {
		char *cmd;
		gets(cmd);

		if (strcmp(cmd, "exit") == 0)
			break;

		pid_t pid = 0;

		if (cmd && *cmd && cmd[strlen(cmd) - 1] == '&') {
			cmd[strlen(cmd)-1] = '\0';
			pid = fork();
		}

		if (pid == 0) {
			system(cmd);
			printf("> ");
			break;
		}
	}
	return 0;
}