#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>

int main() {
    int f = open("ex1.txt", O_RDWR);

    struct stat s = {};
    if (fstat(f, &s)) 
        printf("fstat error\n");

    off_t size = s.st_size;
    
    char *addr;
    addr = mmap(NULL, size, PROT_READ|PROT_WRITE, MAP_SHARED, f, 0);

    strncpy(addr, "This is a nice day", 18);
    return 0;
}