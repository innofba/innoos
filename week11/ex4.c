#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

int main() {
    int f1;
    int f2;
    char *file1;
    char *file2;
    size_t fsize;

    f1 = open("ex1.txt", O_RDONLY);
    fsize = lseek(f1, 0, SEEK_END);

    file1 = mmap(NULL, fsize, PROT_READ, MAP_PRIVATE, f1, 0);

    f2 = open("ex1.memcpy.txt", O_RDWR | O_CREAT, 0666);
    ftruncate(f2, fsize);

    file2 = mmap(NULL, fsize, PROT_READ | PROT_WRITE, MAP_SHARED, f2, 0);
    memcpy(file2, file1, fsize);

    munmap(file1, fsize);
    munmap(file2, fsize);

    close(f1);
    close(f2);
    return 0;
}