#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main() {
    char buff[5];

    setvbuf(stdout, buff, _IOLBF, sizeof(buff));

    printf("H"); sleep(1);
    printf("e"); sleep(1);
    printf("l"); sleep(1);
    printf("l"); sleep(1);
    printf("o"); sleep(1);

    putchar('\n');

    return 0;
}