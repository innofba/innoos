#include <stdio.h>
#include <stdlib.h>

void *myrealloc(void *orig, size_t origlen, size_t newlen) {
    if (newlen == 0) {
        free(orig);
        return NULL;
    }

    if (!orig)
        return malloc(newlen);

    if (origlen >= newlen) {
        return orig;
    }

    void *new = malloc(newlen);
    memcpy(new, orig, newlen);
    free(orig);
    return new;
}

int main() {
    int n1=0;
    scanf("%d",&n1);

    int* a1 = malloc(n1*sizeof(int));
    int i;
    for(i=0; i<n1; i++){
        a1[i]=100;
        printf("%d ", a1[i]);
    }

    int n2=0;
    scanf("%d",&n2);

    a1 = myrealloc(a1, n1, n2*sizeof(int));

    if (n2 > n1) {
        for (int i = n1; i < n2; ++i) {
            a1[i] = 0;
        }
    }
    

    for(i=0; i<n2;i++){
        printf("%d ", a1[i]);
    }
    printf("\n");
    
    return 0;
}