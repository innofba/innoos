#!/bin/bash

FILE="number"

while true; do
    if [ -f $FILE.lock ];
    then
       sleep 1
    else
    	ln $FILE $FILE.lock
		for i in {1..1000}
		do
			X=$(cat $FILE | tail -1)
			echo $(($X + 1)) >> $FILE
		done
		rm $FILE.lock
		break
	fi
done