#include <stdlib.h>
#include <stdio.h>

#include <pthread.h>

#define READY_BUFFER_SIZE 60

int buffer = 0;

pthread_mutex_t mutex;
pthread_cond_t condition;

void *consumer(void *args)
{
	while(1)
	{
		while (buffer < READY_BUFFER_SIZE)
		{
			pthread_mutex_lock(&mutex);
			pthread_cond_wait(&condition, &mutex);
			pthread_mutex_unlock(&mutex);
		}
		sleep(1);
		printf("Consuming %d\n", buffer);
		buffer -= READY_BUFFER_SIZE;
	}
	
	return NULL;
}

void *producer(void *args)
{
	while (1)
	{
		sleep(1);
		buffer++;
		if (buffer >= READY_BUFFER_SIZE)
		{
			printf("Produced %d\n", buffer);
			pthread_cond_signal(&condition);
		}
	}
	return NULL;
}

int main(int argc, char *argv[])
{
	pthread_t tproducer, tconsumer;
	
	pthread_cond_init(&condition, NULL);
	
	int res = pthread_create(&tproducer, NULL, &producer, NULL);
	if (res != 0)
	{
		return res;
	}
	
	res = pthread_create(&tconsumer, NULL, &consumer, NULL);
	if (res != 0)
	{
		return res;
	}
	
	pthread_join(tproducer, NULL);
	pthread_join(tconsumer, NULL);
	
	return 0;
}