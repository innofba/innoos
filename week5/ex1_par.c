#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#define N 5

pthread_t tid[N];

void* doInThread(int i)
{
	printf("thread %d prints message\n", i);
	sleep(1);
}

int main(void)
{
	for (int i = 0; i < N; ++i)
	{
		int err = pthread_create(&(tid[i]), NULL, &doInThread, i);
		if (err != 0) {
			printf("thread creation error %d\n", err);
		} else {
			printf("thread %d created\n", i);
		}
	}
	for (int i = 0; i < N; ++i)
	{
		pthread_join(tid[i], NULL); 
		printf("thread %d exits\n", i);
	}
	return 0;
}