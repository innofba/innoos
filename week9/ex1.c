#include <stdio.h>
#include <stdlib.h>
#include <sys/resource.h>
#include <string.h>

#define FRAMES_NUMBER 10
#define MAX_VAL 4294967295

int main() {
    FILE *file = fopen("data", "r");

    int hits = 0;
    int misses = 0;

    int pages[FRAMES_NUMBER];
    unsigned int access[FRAMES_NUMBER];

    for(int i =0; i < FRAMES_NUMBER; i++) {
        pages[i] = -1;
        access[i] = 0;
    }

    unsigned int time = 0;

    int page;
    while (fscanf(file, "%d", &page) == 1) {
        int hit = 0;
        time++;
        for (int i = 0; i < FRAMES_NUMBER; ++i) {
            if (page == pages[i]) {
                access[i] = time;
                hit = 1;
                hits++;
                break;
            }
        } 
        if (hit)
            continue;
        misses++;
        int replace_page = -1;
        int oldest = MAX_VAL;

        for (int i = 0; i < FRAMES_NUMBER; ++i) {
            if (access[i] < oldest) {
                oldest = access[i];
                replace_page = i;
            }
        }

        access[replace_page] = time;
        pages[replace_page] = page;

    }

    printf("H: %d; M: %d\n", hits, misses);
    printf("%f\n", hits*1.0/misses);

    return 0;
}
