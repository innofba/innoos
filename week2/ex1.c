#include <stdio.h>
#include <float.h>
#include <limits.h>

int main()
{
	int i = INT_MAX;
	float f = FLT_MAX;
	double d = DBL_MAX;

   printf("int: size: %lu; value: %d\n", sizeof(i), i);
   printf("float: size: %lu; value: %f\n", sizeof(f), f);
   printf("double: size: %lu; value: %f\n", sizeof(d), d);

   return 0;
}